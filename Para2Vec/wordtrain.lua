require("io")
require("os")
require("paths")
require("torch")
require("sys")

--local word = torch.class("word")
function split(input, sep)
    if sep == nil then
        sep = "%s"
    end
    local t = {}; local i = 1
    for str in string.gmatch(input, "([^"..sep.."]+)") do
        t[i] = str; i = i + 1
    end
    return t
end



file = io.open("onlyTweets_test.txt", "r")


vocab=torch.load('vocabulary.txt','ascii')
features=torch.load('test_new1.txt','ascii')
file = io.open('onlyTweets_train.txt', "r")
file_new = io.open("feat_train_new.csv","w")
file_label = io.open("labels_train.txt","r")
paravec=torch.Tensor(1,100)
--train_para=torch.Tensor(7606,100)
for i=1,100 do
	file_new:write(string.format("%s,", i))
end
	file_new:write(string.format("label\n", i))
c=1
while true do
	line = file:read()
        if line == nil then break end
	--print (line)
	words = split(line," ")
	wordsvec=torch.Tensor( #words , 100)
	for i,w in ipairs(words) do
		word_idx=vocab[w]
		if word_idx ~= nil then
			--print (w)
			wordsvec[i]=features[word_idx]			
			--print (wordsvec[i])	
		end		
	end
	paravec=torch.mean(wordsvec,1)
	--train_para[c]=paravec
	for i=1,paravec:size(2) do
		file_new:write(string.format("%6f,", paravec[{1,i}]))
	end
	file_new:write(string.format("%d", file_label:read()))
	file_new:write("\n")	
	c = c+1
--	print (paravec)
end
file:close()
print (c)
--torch.save('features_test.csv', train_para ,'ascii')
--print (#paravec)
--print (#wordsvec)





















