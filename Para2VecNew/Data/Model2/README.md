This folder contains data generated entirely during  running of this project.

In model2, Para2Vec is implemented using a two different tables for word vectors and paragraph vectors.

1. Gen_Dataset/:
This folder contains the dataset used in the model1, which is generated using vocabulary from the giventraining and test data.

2. Model/:
This folder contain the output of the neural network(Feature vectors for both tarin and test dataset).

3. CSV/:
This folder contain CSV file of both train and test file, with the word vectors and labels, which is further used in classifier.

4. Predicted_Labels/:
This folder contain the final output of the classifier.
