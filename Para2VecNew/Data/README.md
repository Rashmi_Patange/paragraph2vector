This folder contains data generated entirely during  running of this project.


1. Glove/:
This folder contain global pretrained word vetors.

2. Id_Tweets/:
This folder contain the file for Paragrapd_Id and Tweets of both training and testing.

3. Labels/:
This folder contain files for the labels of both train and test file.

4. Main_Dataset/:
This folder contain the datasets for both train and test data.

5. Model1/:
This folder contain all the data required in the Model1.

6. Model2/:
This folder contain all the data required in the Model2.

7. Para_Id/:
This folder contain Paragrapd_Id file for both train and test data.

8. Voacb/:
This folder contain the whole vocabulary of the data.
