require 'torch'
require 'nn'

--[[
Dataset:
i like nlp
i hate dl
]]--

function split(input, sep)
    if sep == nil then
        sep = "%s"
    end
    local t = {}; local i = 1
    for str in string.gmatch(input, "([^"..sep.."]+)") do
        t[i] = str; i = i + 1
    end
    return t
end

vocab=torch.load('../../../Data/Vocab/vocab.txt','ascii')
dataset=torch.load('../../../Data/Model2/Gen_Dataset/Dataset_tensor','ascii')
word_file2=io.open('../../../Data/Glove/glove.twitter.27B.100d.txt', "r")
word_file=io.open('../../../Data/Glove/glove.twitter.27B.50d.txt', "r")


-- Step 2: Define constants
dataset_size=162432
vocab_size=162432
para_size=162432
word_embed_size=50
learning_rate=0.1
window_size=2
max_epochs=5
c=2
numhid2 = 20
concat_size=(2*c +1) * word_embed_size


function dataset:size()  return 10 end 
-- Step 4: Define your model
wordLookup=nn.LookupTable(vocab_size,word_embed_size)
paraLookup=nn.LookupTable(para_size,word_embed_size)
model=nn.Sequential()
model:add(nn.ParallelTable())
model.modules[1]:add(paraLookup)
model.modules[1]:add(wordLookup)
--model.modules[1]:add(mm.Mean())
model:add(nn.JoinTable(1)) 
model:add(nn.Reshape(concat_size))
model:add( nn.Linear(concat_size,numhid2));
model:add( nn.Sigmoid());                       -- activation function.
model:add( nn.Linear(numhid2,vocab_size) );     
--model:add(nn.Linear(concat_size,vocab_size))
model:add(nn.LogSoftMax())

-- Step 5: Define the loss function (Negative log-likelihood)
criterion=nn.ClassNLLCriterion()

-- Step 6: Define the trainer
trainer=nn.StochasticGradient(model,criterion)
trainer.learningRate=learning_rate
trainer.maxIteration=max_epochs

print('Word Lookup before learning')
--print(lookup.weight)

for line in word_file:lines() do	
	words=split(line)
	--print(words)
	ind=vocab[words[1]]
	if ind ~= nil then
		for i=2,word_embed_size+1 do
			wordLookup.weight[ind][i-1]=words[i]
		end
	end
end

-- Step 7: Train the model with dataset
trainer:train(dataset)

-- Step 8: Get the word embeddings
print('\nWord Lookup after learning')
--print(lookup.weight)
--print(wordLookup.weight[vocab['the']])

torch.save('../../../Data/Model2/Model/wordlookup',wordLookup.weight,'ascii')
torch.save('../../../Data/Model2/Model/paralookup',paraLookup.weight,'ascii')

