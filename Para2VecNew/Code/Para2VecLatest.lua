require 'torch'
require 'nn'
vocab=torch.load('vocab_new.txt','ascii')
dataset=torch.load('Dataset_train_tensor','ascii')

-- Step 2: Define constants
vocab_size=34039
word_embed_size=10
learning_rate=0.01
max_epochs=5

function dataset:size() return 138512 end

-- Step 4: Define your model
model=nn.Sequential()
model:add(nn.LookupTable(vocab_size,word_embed_size))
model:add(nn.Mean())
model:add(nn.Linear(word_embed_size,vocab_size))
model:add(nn.LogSoftMax())

-- Step 5: Define the loss function (Negative log-likelihood)
criterion=nn.ClassNLLCriterion()

-- Step 6: Define the trainer
trainer=nn.StochasticGradient(model,criterion)
trainer.learningRate=learning_rate
trainer.maxIteration=max_epochs

print('Word Lookup before learning')
--print(model.modules[1].weight)

-- Step 7: Train the model with dataset
trainer:train(dataset)

-- Step 8: Get the word embeddings
print('\nWord Lookup after learning')
--print(model.modules[1].weight)
torch.save('model', model.modules[1],'ascii')
