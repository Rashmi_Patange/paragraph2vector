This folder contains the source code of the project.

1. Tokenize/ :
To tokenize the tweets from train and test data. The tokenizer is specially used for twitter data which can recognise the emoticas,      links  etc.

2. Model1/ :
This folder contains a code to train the model for the given dataset and vocabulary using neural networks. 
Two methods were tried: one where the vectors were initailized randomly and other where vectors are initialized with theglove word vectors.
The paragraph ids are included in the vocab itself.

3. Model2/ :
This has a slight modification in implementation than model1 where we store the paragraph vectors seperate from the word vectors.

4. Classifier/ :
This folder contains a code in python to classify the data. linearSVM classification technique was used for classification.




