from sklearn.datasets import load_files
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.grid_search import GridSearchCV
from sklearn.pipeline import make_pipeline
from sklearn.svm import LinearSVC
from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import cross_val_score
from sklearn.metrics import classification_report
import pandas as pd
#data = load_files('txt_sentoken/')
train_dataframe = pd.read_csv("../../Data/Model1/CSV/new_train_para_features_100.csv")
labels_dataframe = pd.read_csv("../../Data/Labels/labels_test.txt")

test_dataframe = pd.read_csv("../../Data/Model1/CSV/new_test_para_features_100.csv")

feature_cols = test_dataframe.columns
print feature_cols

X_train = train_dataframe[feature_cols]
Y_test = labels_dataframe['label']

X_train, X_test, y_train, y_test = train_test_split(X_train,Y_test, test_size=0.33, random_state=0)

pipeline = make_pipeline(LinearSVC(class_weight='auto'))

parameters = {
        'linearsvc__C':[.1,.01,.001,.0001,.000001,.00000001]
        }

grid_search = GridSearchCV(pipeline, parameters, n_jobs=-1) 

grid_search.fit(X_train, y_train)

print("Best score: %0.3f" % grid_search.best_score_)
print("Best parameters iset:")
best_parameters = grid_search.best_estimator_.get_params()
for param_name in sorted(parameters.keys()):
    print("\t%s: %r" % (param_name, best_parameters[param_name]))

print "The model was trained on the full development set"
print "the scores are going to be computed with the evaluation set"
scores = cross_val_score(grid_search, X_test, y_test, cv=5)

print scores.mean(), scores.std()

y_pred = grid_search.predict(X_test)

print classification_report(y_test, y_pred)
