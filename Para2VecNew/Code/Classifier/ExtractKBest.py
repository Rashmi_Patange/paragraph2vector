'''
ExtractKBest: 	Extracts K best features and displays a graph.
Input Data: 	Train and Test file(Files should be in the same folder)
Output: 	Graph of features that closely corelate to Cover_Type

'''
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from pandas import *
from sklearn.linear_model import *
from sklearn.feature_selection import SelectKBest, f_classif

forest = pandas.read_csv("../../Data/Model1/CSV/new_train_para_features_150.csv")
forest_test = pandas.read_csv("../../Data/Model1/CSV/new_test_para_features_100")
#print(forest.describe())


# The columns we'll use to predict the target
predictors = forest_test.columns.values
print predictors
selector = SelectKBest(f_classif, k=20)
selector.fit(forest[predictors], forest["label"])

# Get the raw p-values for each feature, and transform from p-values into scores
scores = -np.log10(selector.pvalues_)
#np.asarray(predictors.get_feature_names())[selector.get_support()]

# Plot the scores.  See how "Pclass", "Sex", "Title", and "Fare" are the best?
plt.bar(range(len(predictors)), scores)
plt.xticks(range(len(predictors)), predictors, rotation='vertical')
plt.show()
