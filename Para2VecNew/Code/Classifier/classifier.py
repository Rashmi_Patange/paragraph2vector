import pandas as pd
from sklearn import svm
from sklearn import ensemble
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Imputer
from sklearn.metrics import *
from sklearn.ensemble import ExtraTreesClassifier


if __name__ == "__main__":
  path_train = "../../Data/Model1/CSV/new_train_para_features_100.csv"
  path_test = "../../Data/Model1/CSV/new_test_para_features_100.csv"
  path_submission = "../../Data/Model1/Predicted_Labels/predicted.csv"
  path_test_label = "../../Data/Labels/labels_test.txt"

  #Y_test = create from file path_test_label

  training_dataframe = pd.read_csv(path_train)
  testing_dataframe = pd.read_csv(path_test)
  labels_dataframe = pd.read_csv(path_test_label)

  feature_cols = testing_dataframe.columns
  print feature_cols
  
  print training_dataframe.columns
  
  X_train = training_dataframe[feature_cols]
  X_test = testing_dataframe[feature_cols]
  y = training_dataframe['label']
  Y_test = labels_dataframe['label']
  #X_train.fillna(0)
  #X_test.fillna(0)	  
 
  #print y
  #print Y_test
  X_train = Imputer().fit_transform(X_train)
  X_test = Imputer().fit_transform(X_test)
  
  clf = svm.LinearSVC(C=100)
  #clf =  Pipeline([("scale", StandardScaler()),("clf", svm.LinearSVC())])
  #clf = ensemble.ExtraTreesClassifier(n_estimators=200,n_jobs=2,random_state=0)

  clf.fit(X_train, y)



# Train the algorithm using all the training data
#alg.fit(forest[predictors], forest["Cover_Type"])

# Make predictions using the test set.
predictions = clf.predict(X_test)

# Create a new dataframe with only the columns Kaggle wants from the dataset.
submission = pd.DataFrame({
		"label": predictions,        
    })
submission.to_csv("../../Data/Model1/Predicted_Labels/predicted.csv", index=False)
print 
print "Accuracy = "+str(accuracy_score(Y_test, predictions))
print 
print "F1 Score = "+str(f1_score(Y_test, predictions, average='macro'))
print
print "Confusion Matrix = "
print confusion_matrix(Y_test, predictions, labels=None)
