'''
Code: 		Predits Forest_Cover for the Forest cover type dataset
Input Data: 	Train and Test files(Files should be in the same folder)
Output:		Kaggle.csv(predicted labels)
'''

from pandas import *
from sklearn import ensemble
from sklearn.linear_model import *
# Sklearn also has a helper that makes it easy to do cross validation
from sklearn.cross_validation import KFold
from sklearn import cross_validation
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.svm import *
from sklearn.metrics import *

forest = pandas.read_csv("../../Data/Model1/CSV/new_train_para_features_100")
forest_test = pandas.read_csv("../../Data/Model1/CSV/new_test_para_features_100")

#forest = pandas.read_csv("train_para_features.csv")
#forest_test = pandas.read_csv("test_para_features.csv")
test = pandas.read_csv("../../Data/Labels/labels_test.txt")

Y_test = test['label']
# The columns we'll use to predict the target
predictors = [col for col in forest_test.columns.values if col not in ["7", "27","47","50","97"]]
#predictors = [col for col in forest_test.columns.values if col in ["9", "35","61","63","75","81"]]

#predictors = forest_test.columns.values
# Initialize our algorithm
#alg = LogisticRegression(random_state=2)
#alg = ensemble.RandomForestClassifier(random_state=0, n_estimators=200, n_jobs = -1)
#alg = ensemble.ExtraTreesClassifier(random_state=0, n_estimators=200, n_jobs = -1)
print predictors
alg = LinearSVC(C=10,class_weight="auto",verbose=1)
# Train the algorithm using all the training data
alg.fit(forest[predictors], forest["label"])

# Make predictions using the test set.
predictions = alg.predict(forest_test[predictors])

# Create a new dataframe with only the columns Kaggle wants from the dataset.
submission = pandas.DataFrame({
		"label": predictions 
    })
submission.to_csv("../../Data/Model1/Predicted_Labels/predicted.csv", index=False)
print(accuracy_score(Y_test, predictions))
print(f1_score(Y_test, predictions))
print("Done");
