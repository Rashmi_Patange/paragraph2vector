require("io")
require("os")
require("paths")
require("torch")
require("sys")

vocab=torch.load('vocab_new.txt','ascii')
dataset={}
function split(input, sep)
    if sep == nil then
        sep = "%s"
    end
    local t = {}; local i = 1
    for str in string.gmatch(input, "([^"..sep.."]+)") do
		if vocab[str] ~= nil then
        	t[i] = str
			i = i + 1
		end
    end
    return t
end
c=2
count=0
f=io.open("train_id_tweets.txt", "r")
i=1
for line in f:lines() do	
	words=split(line)
	line_size=#words
	
	count=count+line_size-4
	if (2*c +1) <= line_size then			
	
		for k=c+2, line_size-c  do
			input1=torch.Tensor((2*c)+1)
			ptr=1
			input1[ptr]=vocab[words[1]]
			ptr=ptr+1
			for l=c, 1, -1 do
				input1[ptr]= vocab[words[k-l]]
				ptr=ptr+1
			end
			
			for l=1, c do
				input1[ptr]= vocab[words[k+l]]
				ptr=ptr+1
			end


		output1=torch.Tensor{vocab[words[k]]}
		dataset[i]={input1,output1}
		i=i+1
		end	
		
	end
	
end
print(count)
--torch.save('Dataset_train_tensor', dataset,'ascii')

