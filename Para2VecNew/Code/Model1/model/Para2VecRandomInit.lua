require 'torch'
require 'nn'
vocab=torch.load('../../../Data/Vocab/vocab.txt','ascii')
dataset=torch.load('../../../Data/Model1/Gen_Dataset/Dataset_tensor','ascii')

-- Step 2: Define constants
vocab_size=38362
word_embed_size=50
learning_rate=0.1
max_epochs=2
dataset_size=162432
numhid2=50
function dataset:size() return dataset_size end

-- Step 4: Define your model
model=nn.Sequential()
model:add(nn.LookupTable(vocab_size,word_embed_size))
model:add(nn.Mean())
--model:add(nn.Linear(word_embed_size,vocab_size))
model:add( nn.Linear(word_embed_size,numhid2));
model:add( nn.Sigmoid());                       -- activation function.
model:add( nn.Linear(numhid2,vocab_size) );
model:add(nn.LogSoftMax())

-- Step 5: Define the loss function (Negative log-likelihood)
criterion=nn.ClassNLLCriterion()

-- Step 6: Define the trainer
trainer=nn.StochasticGradient(model,criterion)
trainer.learningRate=learning_rate
trainer.maxIteration=max_epochs

print('Word Lookup before learning')
--print(model.modules[1].weight)

-- Step 7: Train the model with dataset
trainer:train(dataset)

-- Step 8: Get the word embeddings
print('\nWord Lookup after learning')
--print(model.modules[1].weight)
torch.save('../../../Data/Model1/Model/model_random_weights', model.modules[1].weight,'ascii')
