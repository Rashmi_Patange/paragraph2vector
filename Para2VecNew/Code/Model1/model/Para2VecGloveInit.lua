require 'torch'
require 'nn'

function split(input, sep)
    if sep == nil then
        sep = "%s"
    end
    local t = {}; local i = 1
    for str in string.gmatch(input, "([^"..sep.."]+)") do
        t[i] = str; i = i + 1
    end
    return t
end

vocab=torch.load('../../../Data/Vocab/vocab.txt','ascii')
print (vocab)
dataset=torch.load('../../../Data/Model1/Gen_Dataset/Dataset_tensor','ascii')
word_file=io.open('../../../Data/Glove/glove.twitter.27B.100d.txt', "r")

-- Step 2: Define constants
vocab_size=38362
word_embed_size=100
learning_rate=0.1
max_epochs=2
dataset_size=162432
numhid2=20

function dataset:size() return dataset_size end

-- Step 4: Define your model
model=nn.Sequential()

lookup=nn.LookupTable(vocab_size,word_embed_size)
model:add(lookup)
model:add(nn.Mean())

model:add( nn.Linear(word_embed_size,numhid2));
model:add( nn.Sigmoid());                       -- activation function.
model:add( nn.Linear(numhid2,vocab_size) );

--model:add(nn.Linear(word_embed_size,vocab_size))
model:add(nn.LogSoftMax())

-- Step 5: Define the loss function (Negative log-likelihood)
criterion=nn.ClassNLLCriterion()

-- Step 6: Define the trainer
trainer=nn.StochasticGradient(model,criterion)
trainer.learningRate=learning_rate
trainer.maxIteration=max_epochs

print('Word Lookup before learning')
--print(lookup.weight)
--print(lookup.weight[vocab['the']])
print("here\n")
for line in word_file:lines() do	
	words=split(line)
	--print(words)
	ind=vocab[words[1]]
	if ind ~= nil then
		for i=2,word_embed_size+1 do
			lookup.weight[ind][i-1]=words[i]
		end
	end
end

-- Step 7: Train the model with dataset
trainer:train(dataset)

-- Step 8: Get the word embeddings
print('\nWord Lookup after learning')
--print(lookup.weight)
print(lookup.weight[vocab['the']])
torch.save('../../../Data/Model1/Model/model_init_weights', lookup.weight,'ascii')
