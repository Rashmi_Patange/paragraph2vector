This folder contains the source code for model1

In model1, Para2Vec is implemented using a single table for word vectors and paragraph vectors.
The vocabulary also includes the paragraph ids.

1. CreateVocab/ :
This folder contains code for creating the vocabulary from the tokenized train and test files.

2. CreateDataset/ :
This folder contains a code that creates a dataset with context c where a word i is considered as output and the words from i-c to i-1 and i+1 to i-1 are considered to be the input along with the paragraph id for each word.

3. Model/ :
This folder contains a code to train the model for the given dataset and vocabulary using neural networks. 
Two methods were tried: one where the vectors were initailized randomly and other where vectors are initialized with theglove word vectors.
The paragraph ids are included in the vocab itself.

4. CreateCSV/ :
This folder contains code to create csv file for train and test datasets, with the word vectors and class labels. The output files are then used as input to the classifier.




