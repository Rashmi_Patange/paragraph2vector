require("io")
require("os")
require("paths")
require("torch")
require("sys")

model_output=torch.load('../../../Data/Model1/Model/model_new_weights_sig','ascii')
train_id=io.open('../../../Data/Para_Id/train_id.txt','r')
vocab=torch.load('../../../Data/Vocab/vocab.txt','ascii')
labels=io.open('../../../Data/Labels/labels_train.txt','r')
file_new = io.open("../../../Data/Model1/CSV/new_train_para_features_100","w")

features={}
i=1
vocab_size=38362
word_embed_size=100
learning_rate=0.01
max_epochs=5

for j=1,word_embed_size do
	file_new:write(string.format("%s,", j))
end
file_new:write(string.format("label\n"))

for id in train_id:lines() do
	ind=vocab[id]
	features[i]=model_output[ind]
	i=i+1
	for j=1,word_embed_size do
		file_new:write(string.format("%6f,", features[i][j]))
	end
	file_new:write(string.format("%d", labels:read()))
	file_new:write("\n")	
end
file_new:close()
