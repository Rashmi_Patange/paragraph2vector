require("io")
require("os")
require("paths")
require("torch")
require("sys")

model_output=torch.load('../../../Data/Model1/Model/model_random_weights','ascii')
train_id=io.open('../../../Data/Para_Id/train_id.txt','r')
vocab=torch.load('../../../Data/Vocab/vocab.txt','ascii')
--labels=io.open('labels_train.txt','r')
file_new = io.open("../../../Data/Model1/CSV/glove_test_para_features_sig.csv","w")

features={}
i=1

word_embed_size=100

for j=1,word_embed_size-1 do
	file_new:write(string.format("%s,", j))
end
file_new:write(string.format("%s\n",word_embed_size))

for id in train_id:lines() do

	ind=vocab[id]
	--print(ind)
	features[i]=model_output[ind]
	--print(features[i])
	
	for j=1,word_embed_size-1 do
		file_new:write(string.format("%6f,", features[i][j]))
	end
	file_new:write(string.format("%6f", features[i][word_embed_size]))
	--file_new:write(string.format("%d", labels:read()))
	file_new:write("\n")	
	i=i+1
end
file_new:close()
