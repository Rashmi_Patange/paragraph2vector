require("io")
require("os")
require("paths")
require("torch")
require("sys")

model_output=torch.load('model','ascii')
train_id=io.open('train_id.txt','r')
vocab=torch.load('vocab_new.txt','ascii')
labels=io.open('labels_train.txt','r')
file_new = io.open("feat_train_new.csv","w")

features={}
i=1
vocab_size=34039
word_embed_size=10
learning_rate=0.01
max_epochs=5

for j=1,word_embed_size do
	file_new:write(string.format("%s,", j))
end
file_new:write(string.format("label\n"))

for id in train_id:lines() do
	ind=vocab[id]
	features[i]=model_output[ind]
	i=i+1
	for j=1,word_embed_size do
		file_new:write(string.format("%6f,", features[i][j]))
	end
	file_new:write(string.format("%d", labels:read()))
	file_new:write("\n")	
end