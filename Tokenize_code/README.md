## TOKENIZER 

**train.csv**: Total tweets: 7605 Total tokens: 184142

**test.csv**: Total tweets: 1317 Total tokens: 31822

	Steps for tokenizing:
	
	
	1. Run twokenize.java with 2 input arguments.
		* Input_filename
		* Output_filename
		Run for both train and test files.


	2. Pass the 2 output files created by the java code to the bash script to get only the tweets without id and class.This creates two files 		
		* onlyTweets_train.txt: Contains only tweets from training data
		* onlyTweets_test.txt: Contains only tweets from testing data

			Run createTweets.sh <output_filename_train> <output_filename_test>
	
